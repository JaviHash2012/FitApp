﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using FitApp.Helpers;
using FitApp.Model;
namespace FitApp
{
    
    [Activity(Label = "Login", Theme = "@style/Theme.AppCompat.Light.NoActionBar", MainLauncher = true)]
    public class Login : AppCompatActivity
    {
        /// <summary>
        /// Controles
        /// </summary>
        private EditText ed_email;
        private EditText ed_password;
        private Button btnSingIn;
        private Button btnSingUpFB;
        private TextView tv_question;

        /// <summary>
        /// Variables para el inicio de la sesion
        /// </summary>
        private HelperUsuario helperUsuario;
        private Usuario usuario;
        private int idUsuario;

        /// <summary>
        /// Variables para manipular las preferencias compartidas
        /// </summary>
        private ISharedPreferences sharedPreferencesUserInfo;
        private ISharedPreferencesEditor sharedPreferencesEditor;

        /// <summary>
        /// Variable para el cambio de activity
        /// </summary>
        private Intent siguienteActivity;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here

            SetContentView(Resource.Layout.Login);
            InicializarComponentes();
        }

        private void InicializarComponentes()
        {
            sharedPreferencesUserInfo = Application.Context.GetSharedPreferences("InformacionUsuario", FileCreationMode.Private);
            sharedPreferencesEditor = sharedPreferencesUserInfo.Edit();

            helperUsuario = new HelperUsuario();

            helperUsuario.CreateDatabase();

            ed_email = FindViewById<EditText>(Resource.Id.ed_user);
            ed_password = FindViewById<EditText>(Resource.Id.ed_password);
            tv_question = FindViewById<TextView>(Resource.Id.tv_signUp);

            btnSingIn = FindViewById<Button>(Resource.Id.btn_signIn);
            btnSingUpFB = FindViewById<Button>(Resource.Id.btn_signInFacebook);

            btnSingIn.Click += IniciarSesion;
            tv_question.Click += IniciarRegistro;
        }

        private void IniciarRegistro(object sender, EventArgs args)
        {
            Intent nextActivity = new Intent(this, typeof(SignUp));
            StartActivity(nextActivity);
        }

        private void IniciarSesion(object sender, System.EventArgs e)
        {
            usuario = new Usuario
            {
                Contrasena = ed_password.Text,
                Correo = ed_email.Text
            };

            if (helperUsuario.Login(usuario))
            {
                //Obtenemos el id del usuario que se ha loggeado
                idUsuario = helperUsuario.ObtenerIdUsuario(usuario);
                //Guardamos el id en las preferencias compartidas para hacer uso de el mientras la sesion este activa
                sharedPreferencesEditor.PutInt("Id_Usuario", idUsuario);
                //Indicamos que la sesion estara activa para este usuario guardando en la clave logged como verdadero
                sharedPreferencesEditor.PutBoolean("logged", true);
                //Aplicamos los cambios
                sharedPreferencesEditor.Apply();

                siguienteActivity = new Intent(this, typeof(MainActivity));
                StartActivity(siguienteActivity);
            }
            else
            {
                Android.Support.V7.App.AlertDialog.Builder alertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
                alertDialog.SetTitle("Incorrecto");
                alertDialog.SetMessage("La contraseña y el correo son incorrectos");
                alertDialog.SetNeutralButton("OK", delegate {
                    alertDialog.Dispose();
                });

                alertDialog.Show();
            }
        }
    } 
}
