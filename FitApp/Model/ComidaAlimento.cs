﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;

namespace FitApp.Model
{
    [Table("ComidasAlimentos")]
    public class ComidaAlimento
    {
        [PrimaryKey, AutoIncrement]
        public int Id_ComidaAlimento
        {
            get;
            set;
        }
        public int Id_Comida
        {
            get;
            set;
        }
        public int Id_Alimento
        {
            get;
            set;
        }
    }
}