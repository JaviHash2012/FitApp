﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.View;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using FitApp.Helpers;
using FitApp.Model;

namespace FitApp
{
    [Activity(Label = "Registro", Theme = "@style/Theme.AppCompat.Light")]
    public class SignUp : AppCompatActivity
    {
        private Button btn_signUp;
        private EditText ed_name_signUp;
        private EditText ed_email_signUp;
        private EditText ed_password_signUp;
        private EditText ed_height_signUp;
        private EditText ed_initial_weight_signUp;
        private EditText ed_objective_weight_signUp;
        private EditText ed_age_signUp;
        private Spinner sp_objectives_signUp;
        private Spinner sp_gender_signUp;
        private Spinner sp_activity_level_signUp;

        private ArrayAdapter genderAdapter;
        private ArrayAdapter activitylevelAdapter;
        private ArrayAdapter objectiveAdapter;

        private HelperUsuario helperUsuario;
        private HelperFormulas helperFormulas;

        private Usuario usuario;
        private Objetivo objetivo;

        

        private int idUsuario;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.SignUp);
            Init();
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    Finish();
                    return true;

                default:
                    return base.OnOptionsItemSelected(item);
            }
        }

        public override void OnBackPressed()
        {
            base.OnBackPressed();
            Finish();
        }

        protected override void OnResume()
        {
            base.OnResume();
            btn_signUp.Click += RegistrarUsuario;
        }

        private bool ValidarCampos()
        {
            if (ed_age_signUp.Text.Equals("") || ed_email_signUp.Equals("") || ed_height_signUp.Text.Equals("") ||
                ed_initial_weight_signUp.Equals("") || ed_name_signUp.Equals("") || ed_objective_weight_signUp.Equals("") ||
                ed_password_signUp.Equals(""))
                return false;
            else
                return true;
        }

        private void RegistrarUsuario(object sender, EventArgs args)
        { 
            if (ValidarCampos())
            {
                int edad = Convert.ToInt32(ed_age_signUp.Text);
                double peso = Convert.ToDouble(ed_initial_weight_signUp.Text);
                double estatura = Convert.ToDouble(ed_height_signUp.Text);
                string sexo = sp_gender_signUp.SelectedItem.ToString();
                string actividadFisica = sp_activity_level_signUp.SelectedItem.ToString();
                double ingestaCalorica = helperFormulas.ObtenerIngestaCaloricaAdultos(actividadFisica, sexo, peso, estatura, edad);

                usuario.Nombre_usuario = ed_name_signUp.Text;
                usuario.Contrasena = ed_password_signUp.Text;
                usuario.Correo = ed_email_signUp.Text;
                usuario.Estatura = estatura;
                usuario.Peso = peso;
                usuario.IMC = helperFormulas.CalcularIMC(estatura, peso);
                usuario.Ingesta_calorica = ingestaCalorica;
                usuario.Nivel_actividad_fisica = actividadFisica;
                usuario.Sexo = sexo;
                usuario.Edad = edad;
                usuario.Numero_Tarjeta = 0;
                idUsuario = helperUsuario.InsertUsuario(usuario);
                if (idUsuario != -1)
                {
                    Intent siguiente = new Intent(this, typeof(Login));
                    StartActivity(siguiente);
                }
                else
                {
                    Android.Support.V7.App.AlertDialog.Builder alertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
                    alertDialog.SetTitle("Notificacion");
                    alertDialog.SetMessage("Usuario Fallido");
                    alertDialog.SetNeutralButton("OK", delegate {
                        alertDialog.Dispose();
                    });

                    alertDialog.Show();
                }
            }
            else
            {
                Android.Support.V7.App.AlertDialog.Builder alertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
                alertDialog.SetTitle("Notificacion");
                alertDialog.SetMessage("Faltan Campos");
                alertDialog.SetNeutralButton("OK", delegate {
                    alertDialog.Dispose();
                });

                alertDialog.Show();
            }     
        }

        //Inicializacion de componentes
        //Enlace de Spinners
        private void Init()
        {
            helperFormulas = new HelperFormulas();
            helperUsuario = new HelperUsuario();
            usuario = new Usuario();
            objetivo = new Objetivo();

            btn_signUp = FindViewById<Button>(Resource.Id.btn_signUp);
            ed_name_signUp = FindViewById<EditText>(Resource.Id.ed_name_signUp);
            ed_email_signUp = FindViewById<EditText>(Resource.Id.ed_email_signUp);
            ed_password_signUp = FindViewById<EditText>(Resource.Id.ed_password_signUp);
            ed_height_signUp = FindViewById<EditText>(Resource.Id.ed_height_signUp);
            ed_initial_weight_signUp = FindViewById<EditText>(Resource.Id.ed_initial_weight_signUp);
            ed_objective_weight_signUp = FindViewById<EditText>(Resource.Id.ed_objective_weight_signUp);
            ed_age_signUp = FindViewById<EditText>(Resource.Id.ed_age_signUp);
            sp_objectives_signUp = FindViewById<Spinner>(Resource.Id.sp_objectives_signUp);
            sp_gender_signUp = FindViewById<Spinner>(Resource.Id.sp_gender_signUp);
            sp_activity_level_signUp = FindViewById<Spinner>(Resource.Id.sp_activity_level_signUp);

            genderAdapter = ArrayAdapter.CreateFromResource(this, Resource.Array.array_options_gender, Android.Resource.Layout.SimpleSpinnerItem);
            activitylevelAdapter = ArrayAdapter.CreateFromResource(this, Resource.Array.array_options_activityLevel, Android.Resource.Layout.SimpleSpinnerItem);
            objectiveAdapter = ArrayAdapter.CreateFromResource(this, Resource.Array.array_options_objective, Android.Resource.Layout.SimpleSpinnerItem);

            genderAdapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            activitylevelAdapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            objectiveAdapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);

            sp_activity_level_signUp.Adapter = activitylevelAdapter;
            sp_objectives_signUp.Adapter = objectiveAdapter;
            sp_gender_signUp.Adapter = genderAdapter;
        }
    }
}