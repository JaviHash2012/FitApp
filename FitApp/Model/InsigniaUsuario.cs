﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;

namespace FitApp.Model
{
    [Table("InsigniasUsuarios")]
    class InsigniaUsuario
    {
        [PrimaryKey]
        public int Id_Insignia
        {
            get;
            set;
        }

        [PrimaryKey]
        public int Id_Usuario
        {
            get;
            set;
        }
    }
}