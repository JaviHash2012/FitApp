﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using FitApp.Model;

namespace FitApp.Helpers
{
    class HelperFormulas
    {
        //Indice de masa corporal
        private double imc;
        //Efecto termico de los alimentos
        private double eta;
        //Gasto total energetico
        private double gett;
        //Energia por actividad fisica
        private double eaf;
        //Gasto energetico basal
        private double geb;

        private int cantidad;
        private int calorias;
        private double lipidos;
        private double carbohidratos;
        private double proteinas;

        private AlimentosUsuario alimento;

        public double Imc { get => imc; set => imc = value; }
        public double Eta { get => eta; set => eta = value; }
        public double Gett { get => gett; set => gett = value; }
        public double Eaf { get => eaf; set => eaf = value; }
        public double Geb { get => geb; set => geb = value; }

        public int Cantidad { get => cantidad; set => cantidad = value; }
        public int Calorias { get => calorias; set => calorias = value; }
        public double Lipidos { get => lipidos; set => lipidos = value; }
        public double Carbohidratos { get => carbohidratos; set => carbohidratos = value; }
        public double Proteinas { get => proteinas; set => proteinas = value; }

        public double CalcularIMC(double altura, double peso)
        {
            imc = peso / Math.Pow(altura,2);
            return imc;
        }

        public double ObtenerIngestaCaloricaAdultos(string nivelActividad, string sexo, double peso, double altura, int edad)
        {
            if (sexo.Equals("hombre"))
            {
                switch (nivelActividad)
                {
                    case "sedentario":
                        geb = 66.5 + (13.7 * peso) + (5 * altura) - edad;
                        eta = geb * 0.1;
                        eaf = geb * 0.1;
                        gett = geb + eta + eaf;
                        break;
                    case "ligero":
                        geb = 66.5 + (13.7 * peso) + (5 * altura) - edad;
                        eta = geb * 0.1;
                        eaf = geb * 0.2;
                        gett = geb + eta + eaf;
                        break;
                    case "moderado":
                        geb = 66.5 + (13.7 * peso) + (5 * altura) - edad;
                        eta = geb * 0.1;
                        eaf = geb * 0.3;
                        gett = geb + eta + eaf;
                        break;
                    case "intenso":
                        geb = 66.5 + (13.7 * peso) + (5 * altura) - edad;
                        eta = geb * 0.1;
                        eaf = geb * 0.4;
                        gett = geb + eta + eaf;
                        break;
                }
                return gett;
            }
            else
            {
                switch (nivelActividad)
                {
                    case "sedentario":
                        geb = 665 + (9.6 * peso) + (1.8 * altura) - (edad * 4.7);
                        eta = geb * 0.1;
                        eaf = geb * 0.1;
                        gett = geb + eta + eaf;
                        break;
                    case "ligero":
                        geb = 665 + (9.6 * peso) + (1.8 * altura) - (edad * 4.7);
                        eta = geb * 0.1;
                        eaf = geb * 0.2;
                        gett = geb + eta + eaf;
                        break;
                    case "moderado":
                        geb = 665 + (9.6 * peso) + (1.8 * altura) - (edad * 4.7);
                        eta = geb * 0.1;
                        eaf = geb * 0.3;
                        gett = geb + eta + eaf;
                        break;
                    case "intenso":
                        geb = 665 + (9.6 * peso) + (1.8 * altura) - (edad * 4.7);
                        eta = geb * 0.1;
                        eaf = geb * 0.4;
                        gett = geb + eta + eaf;
                        break;
                }
                return gett;
            }
        }

        public AlimentosUsuario RecalcularMacronutrientes(Alimento alimento, int cantidadNueva)
        {
            cantidad = alimento.Cantidad;
            //Se hacen los calculos basados en una regla de tres tomando como referencia el 
            calorias = (cantidadNueva * alimento.Calorias) / cantidad;
            carbohidratos = (cantidadNueva * alimento.Carbohidratos) / cantidad;
            proteinas = (cantidadNueva * alimento.Proteinas) / cantidad;
            lipidos = (cantidadNueva * alimento.Lipidos) / cantidad;
        
            return new AlimentosUsuario()
            {
                Nombre_Alimento = alimento.Nombre_Alimento,
                Calorias = calorias,
                Lipidos = lipidos,
                Proteinas = proteinas,
                Cantidad = cantidadNueva,
                Carbohidratos = carbohidratos,
                Categoria = alimento.Categoria,
                IdImagen = alimento.IdImagen
            };
        }
    }
}