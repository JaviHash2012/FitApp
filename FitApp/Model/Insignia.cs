﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;
namespace FitApp.Model
{
    [Table("Insignias")]
    class Insignia
    {
        [PrimaryKey, AutoIncrement]
        public int Id_Insignia
        {
            get;
            set;
        }
        public string Nombre_Insignia
        {
            get;
            set;
        }
        public int Imagen
        {
            get;
            set;
        }
        public int Desbloqueado
        {
            get;
            set;
        }
    }
}