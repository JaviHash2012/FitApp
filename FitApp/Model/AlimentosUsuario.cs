﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using SQLite;
namespace FitApp.Model
{
    public class AlimentosUsuario
    {
        [PrimaryKey, AutoIncrement]
        public int Id_AlimentoUsuario
        {
            get;
            set;
        }
        public int Id_Usuario
        {
            get;
            set;
        }
        public string Nombre_Alimento
        {
            get;
            set;
        }
        public string Categoria
        {
            get;
            set;
        }
        public int Cantidad
        {
            get;
            set;
        }
        public int Calorias
        {
            get;
            set;
        }
        public double Proteinas
        {
            get;
            set;
        }
        public double Carbohidratos
        {
            get;
            set;
        }
        public double Lipidos
        {
            get;
            set;
        }
        public int IdImagen
        {
            get;
            set;
        }
    }
}