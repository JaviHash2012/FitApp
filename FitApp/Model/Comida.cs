﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;
namespace FitApp.Model
{
    
    public class Comida
    {
        [PrimaryKey,AutoIncrement]
        public int Id_Comida
        {
            get;
            set;
        }

        public string Nombre_Comida
        {
            get;
            set;
        }

        public DateTime Fecha
        {
            get;
            set;
        }

        public int UsuarioID
        {
            get;
            set;
        } 

        public int IdImagen
        {
            get;
            set;
        }
        
    }
}