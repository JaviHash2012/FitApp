﻿using System;
using SQLite;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.IO;
using Android.Util;
using System.Linq;

using FitApp.Model;

namespace FitApp.Connection
{
    public class Database
    {
        private string folder = Environment.GetFolderPath(Environment.SpecialFolder.Personal);

        /// <summary>
        /// Creacion de la base de datos 
        /// </summary>
        /// <returns>
        /// Retorna un valor falso en caso de que algo haya ido mal
        /// </returns>
        public bool CreateDatabase()
        {
            try
            {
                using (var connection = new SQLiteConnection(Path.Combine(folder, "FIT.db3")))
                {
                    connection.CreateTable<Usuario>();
                    connection.CreateTable<Comida>();
                    connection.CreateTable<Alimento>();
                    connection.CreateTable<ComidaAlimento>();
                    connection.CreateTable<AlimentosUsuario>();
                    connection.CreateTable<Insignia>();
                    connection.CreateTable<Objetivo>();
                    return true;
                }
            }
            catch(SQLiteException ex)
            {
                Log.Info("SQLiteException: ",ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Metodo para insertar nuevos usuarios
        /// </summary>
        /// <param name="usuario">
        /// Es el usuario que se va a insertar en la base de datos
        /// </param>
        /// <returns>
        /// Retorna el ID del ultimo usuario insertado
        /// </returns>
        public int InsertarUsuario(Usuario usuario)
        {
            try
            {
                using (var connection = new SQLiteConnection(Path.Combine(folder,"FIT.db3")))
                {
                    connection.Insert(usuario);
                    return (int)connection.ExecuteScalar<long>("SELECT last_insert_rowid()");
                }
            }
            catch (SQLiteException ex)
            {
                Log.Info("SQliteException:", ex.Message);
                return -1;
            }
        }

        /// <summary>
        /// Metodo para actualizar la informacion de un usuario
        /// </summary>
        /// <param name="usuario">
        /// Contiene toda la nueva informacion del usuario
        /// </param>
        /// <returns>
        /// Retorna falso si algo fue mal de lo contrario retorna verdadero
        /// </returns>
        public bool ActualizarUsuario(Usuario usuario)
        {
            try
            {
                using (var connection = new SQLiteConnection(Path.Combine(folder,"FIT.db3")))
                {
                    connection.Query<Usuario>("UPDATE Usuario set Nombre_usuario=?,Correo=?,Contrasena=?,Estatura=?,Peso=?,IMC=?,Ingesta_calorica=?," +
                                              "Nivel_actividad_fisica=?,Sexo=?,Edad=?,Numero_Tarjeta=? Where Id_Usuario=?", usuario.Nombre_usuario, usuario.Correo, usuario.Contrasena, usuario.Estatura,
                                              usuario.Peso, usuario.IMC, usuario.Ingesta_calorica, usuario.Nivel_actividad_fisica, usuario.Sexo, usuario.Edad, usuario.Numero_Tarjeta, usuario.Id_Usuario);
                    return true;
                }
            }
            catch(SQLiteException ex)
            {
                Log.Info("SQLiteExceptio",ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Metodo para eliminar al usuario
        /// </summary>
        /// <param name="usuario">
        /// Contiene toda la informacion del usuario a eliminar
        /// </param>
        /// <returns>
        /// Retornara falso en caso de que algo haya salido mal, de lo contrario retorna verdadero
        /// </returns>
        public bool ELiminarUsuario(Usuario usuario)
        {
            try
            {
                using (var connection = new SQLiteConnection(Path.Combine(folder, "FIT.db3")))
                {
                    connection.Delete<Usuario>(usuario);
                    return true;
                }
            }
            catch(SQLiteException ex)
            {
                Log.Info("SQLiteExceptio",ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Metodo para obtener un usuario especificado
        /// </summary>
        /// <param name="usuario">
        /// Contiene el Id del usuario a buscar
        /// </param>
        /// <returns>
        /// Regresa el usuario con el Id de busqueda
        /// </returns>
        public List<Usuario> ObtenerUsuario(Usuario usuario)
        {
            try
            {
                using (var connection  = new SQLiteConnection(Path.Combine(folder, "FIT.db3")))
                {
                    return connection.Table<Usuario>().Where(x => x.Id_Usuario.Equals(usuario.Id_Usuario)).ToList();
                }
            }
            catch(SQLiteException ex)
            {
                Log.Info("SQLiteException",ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Metodo para inicio de sesion
        /// </summary>
        /// <param name="usuario">
        /// Contiene el Correo y contraseña para hacer la validacion de inicio de sesion
        /// </param>
        /// <returns>
        /// El usuario registrado con el Correo y contraseña analizados
        /// </returns>
        public List<Usuario> Login(Usuario usuario)
        {
            try
            {
                using (var connection = new SQLiteConnection(Path.Combine(folder, "FIT.db3")))
                {
                    Log.Info(usuario.Correo, usuario.Contrasena);
                    return connection.Table<Usuario>().Where(x => x.Contrasena.Equals(usuario.Contrasena) && x.Correo.Equals(usuario.Correo)).ToList();
                }
            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteException ", ex.Message);
                return null;
            }
        }

        /// <summary>
        /// El metodo inserta un alimento para el banco de datos
        /// El banco de datos es estatico, el usuario no podra modificar algun alimento
        /// </summary>
        /// <param name="alimento">
        /// Contiene la informacion de alimento que se va a insertar
        /// </param>
        /// <returns>
        /// Regresa falso o verdadero dependiendo el resultado si se realizo correcta la inserccion o fallo
        /// </returns>
        public bool InsertarAlimentoBancoDatos(Alimento alimento)
        {
            try
            {
                using (var connection = new SQLiteConnection(Path.Combine(folder,"FIT.db3")))
                {
                    connection.Insert(alimento);
                    return true;
                }
            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteException",ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Obtenemos todos los alimentos de una determinada Categoria de alimentos
        /// </summary>
        /// <param name="alimento">
        /// Contiene la categoria del alimento a buscar
        /// </param>
        /// <returns>
        /// Una lista con todos los alimentos de la categoria especificada
        /// </returns>
        public List<Alimento> ObtenerAlimentosBancoDatos(Alimento alimento)
        {
            try
            {
                using (var connection = new SQLiteConnection(Path.Combine(folder, "FIT.db3")))
                {
                    return connection.Table<Alimento>().Where(x => x.Categoria.Equals(alimento.Categoria)).ToList();
                }
            }
            catch(SQLiteException ex)
            {
                Log.Info("SQLiteException",ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtiene el alimento con toda su informacion con el Nombre especificado
        /// </summary>
        /// <param name="alimento">
        /// Contiene el nombre del alimento a buscar
        /// </param>
        /// <returns>
        /// El alimento que se ha buscado
        /// </returns>
        public List<Alimento> ObtenerALimento(Alimento alimento)
        {
            try
            {
                using (var connection = new SQLiteConnection(Path.Combine(folder, "FIT.db3")))
                {
                    Log.Info(alimento.Categoria, alimento.Nombre_Alimento);
                    return connection.Table<Alimento>().Where(x => x.Nombre_Alimento.Equals(alimento.Nombre_Alimento)).ToList();
                }
            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteException ", ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Inserta un alimento en este caso que haya creado el usuario,
        /// </summary>
        /// <param name="alimento">
        /// Contiene la informacion del alimento creado por el usuario
        /// </param>
        /// <returns>
        /// Regresa el Id del alimento que se insertó, de lo contrario regresa un -1 si ocurrio un error
        /// </returns>
        public int InsertarALimentoCreado(AlimentosUsuario alimento)
        {
            try
            {
                using (var connection = new SQLiteConnection(Path.Combine(folder,"FIT.db3")))
                {
                    connection.Insert(alimento);
                    return (int)connection.ExecuteScalar<long>("SELECT last_insert_rowid()");
                }
            }
            catch(SQLiteException ex)
            {
                Log.Info("SQLiteException",ex.Message);
                return -1;
            }
        }

        /// <summary>
        /// Obtiene la el alimento con el Id especificado
        /// </summary>
        /// <param name="alimento">
        /// Contiene el Id del alimento a buscar
        /// </param>
        /// <returns>
        /// Regresa el alimento que se encontro
        /// </returns>
        public List<AlimentosUsuario> ObtenerAlimentoUsuario(AlimentosUsuario alimento)
        {
            try
            {
                using (var connection = new SQLiteConnection(Path.Combine(folder, "FIT.db3")))
                {
                    return connection.Table<AlimentosUsuario>().Where(x => x.Id_AlimentoUsuario.Equals(alimento.Id_AlimentoUsuario)).ToList();
                }
            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteException ", ex.Message);
                return null;
            }
        }
    }
}