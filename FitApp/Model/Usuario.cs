﻿using System;
using SQLite;
namespace FitApp.Model
{
    /*
     * Cambios:
     * =>Se cambio el campo Estado por el campo Nivel de actividad fisica
     * Fecha: 25/02/2018
     */

    public class Usuario
    {
       
        [PrimaryKey,AutoIncrement]
        public int Id_Usuario{
            get;
            set;
        }

        public string Nombre_usuario
        {
            get;
            set;
        }

        public string Correo
        {
            get;
            set;
        }

        public string Contrasena
        {
            get;
            set;
        }

        public double Estatura
        {
            get;
            set;
        }

        public double Peso
        {
            get;
            set;
        }

        public double IMC
        {
            get;
            set;
        }

        public double Ingesta_calorica
        {
            get;
            set;
        }

        public string Nivel_actividad_fisica
        {
            get;
            set;
        }

        public string Sexo
        {
            get;
            set;
        }

        public int Edad
        {
            get;
            set;
        }

        public int Numero_Tarjeta
        {
            get;
            set;
        }


    }
}
