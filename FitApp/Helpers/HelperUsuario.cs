﻿using System;
using FitApp.Model;
using System.Collections.Generic;
using System.IO;
using Android.App;
using Android.Util;
using System.Linq;
using FitApp.Connection;
using Android.Content;

namespace FitApp.Helpers
{
    public class HelperUsuario
    {

        private Database database;

        public void CreateDatabase(){
            database = new Database();
            database.CreateDatabase();
        }

        public int InsertUsuario(Usuario usuario){
            database = new Database();
            return database.InsertarUsuario(usuario);
        }

        /// <summary>
        /// Intercede entre el modelo y la vista para acceder a la base de datos e iniciar sesion
        /// Valida nuevamente el usuario y contraseña
        /// </summary>
        /// <param name="usuario">
        /// Contiene el correo y la contraseña
        /// </param>
        /// <returns>
        /// Verdadero en caso de que sean correctos los parametros
        /// </returns>
        public bool Login(Usuario usuario)
        {
            List<Usuario> list = new List<Usuario>();
            database = new Database();
            list = database.Login(usuario);
            try
            {
                if (list[0].Correo.Equals(usuario.Correo) && list[0].Contrasena.Equals(usuario.Contrasena))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                Log.Info("Exception ", ex.Message);
                return false;
            }
        }

        public int ObtenerIdUsuario(Usuario usuario)
        {
            List<Usuario> listaUsuarios = new List<Usuario>();
            database = new Database();
            listaUsuarios = database.Login(usuario);
            return listaUsuarios[0].Id_Usuario;
        }
    }
}
