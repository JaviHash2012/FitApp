﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;

namespace FitApp.Model
{
    [Table("Objetivos")]
    public class Objetivo
    {
        [PrimaryKey, AutoIncrement]
        public int Id_objetivo
        {
            get;
            set;
        }

        public string NombreObjetivo
        {
            get;
            set;
        }

        public int Cumplido
        {
            get;
            set;
        }

        public int Id_Usuario
        {
            get;
            set;
        }

    }
}